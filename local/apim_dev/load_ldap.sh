#!/bin/bash

docker run -d --name id_admin -p "9000:80" -v /home/jstubbs/bitbucket-repos/docker_atim/deploy/roles/ldap_apim/files/update_id_admin_settings.sh:/update_id_admin_settings.sh -v $(pwd)/dev.yml:/values.yml -v $(pwd)/dev_passwords:/passwords jstubbs/agave_id_dedicated
docker exec -it id_admin sh /update_id_admin_settings.sh
sleep 2

curl localhost:9000/ous
curl -X POST --data "ou=tenantdev" localhost:9000/ous

curl -X POST --data "username=jdoe&password=abcde&email=jdoe@test.com" localhost:9000/users
curl -X POST --data "username=testuser&password=testuser&email=testuser@test.com" localhost:9000/users
curl -X POST --data "username=testshareuser&password=testshareuser&email=testshareuser@test.com" localhost:9000/users
curl -X POST --data "username=testotheruser&password=testotheruser&email=testotheruser@test.com" localhost:9000/users

docker rm -f id_admin