#!/bin/bash
#
# Update the DS JupyterHub from scratch.
#
# Required params:
# $MNG_IP: the IP of the swarm manager
# $HOST_FILE: either ds_juptyer_cluster_hosts or ds_jupyter_staging_hosts
# $ENV: either jupyterhub or jupyterhub_staging

deployer -i /deploy/host_files/$HOST_FILE /deploy/swarm_1.11_cluster.plbk -e swarm_manager_ip=$MNG_IP

deployer -i /deploy/host_files/$HOST_FILE /deploy/update_jupyterhub.plbk -e jupyterhub_conf_dir=tenants/designsafe/$ENV/ -e remove_notebook_servers=false -e use_swarm=true -e hub_ip_for_notebooks=$MNG_IP -e run_worker_tasks_file=true

