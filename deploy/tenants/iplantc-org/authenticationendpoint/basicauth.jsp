<%@page import="org.wso2.carbon.identity.application.authentication.endpoint.util.CharacterEncoder"%>
<div id="loginTable1" class="identity-box">
    <%
        loginFailed = CharacterEncoder.getSafeText(request.getParameter("loginFailed"));
        if (loginFailed != null) {

    %>
    <div class="alert alert-danger">
                <fmt:message key='<%=CharacterEncoder.getSafeText(request.getParameter
                ("errorMessage"))%>'/>
            </div>
    <% } %>

    <% if (CharacterEncoder.getSafeText(request.getParameter("username")) == null || "".equals
    (CharacterEncoder.getSafeText(request.getParameter("username")).trim())) { %>

        <!-- Username -->
        <div class="form-group">
            <label class="control-label sr-only" for="username" ><fmt:message key='username'/>:</label>

            <div class="controls col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2">
                <input class="form-control" type="text" id='username' name="username" label="User Name" placeholder="Username" size='30'/>
            </div>
        </div>

    <%} else { %>

        <input type="hidden" id='username' name='username' value='<%=CharacterEncoder.getSafeText
        (request.getParameter("username"))%>'/>

    <% } %>

    <!--Password-->
    <div class="form-group">
        <label class="control-label sr-only" for="password"><fmt:message key='password'/>:</label>

        <div class="controls col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2">
            <input type="password" id='password' name="password"  class="form-control" label="password" size='30' placeholder="Password"/>
            <input type="hidden" name="sessionDataKey" value='<%=CharacterEncoder.getSafeText(request.getParameter("sessionDataKey"))%>'/>
            <label class="checkbox" style="margin-top:10px"><input type="checkbox" label="checkbox" id="chkRemember" name="chkRemember"><fmt:message key='remember.me'/></label>
        </div>
    </div>

    <div class="submit col-xs-6 col-xs-offset-3 col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4">
        <input type="submit" value='<fmt:message key='login'/>' class="btn btn-warning btn-block">
    </div>

</div>
<br /><br />

<div class="help-link help-block col-xs-12 col-sm-12 col-md-12" id="helpBlock"><span class="forgot-password"><a href="https://user.cyverse.org/reset/request" target="_blank">Forgot Password</a></span> | <a href="https://user.cyverse.org" title="User Support through Cyverse" target="_blank">Log In Help</a> | <a href="http://status.agaveapi.co" target="_blank" title="Platform Status">Platform Status</div>
    </div>  
