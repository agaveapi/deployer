<!--
~ Copyright (c) 2005-2014, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
~
~ WSO2 Inc. licenses this file to you under the Apache License,
~ Version 2.0 (the "License"); you may not use this file except
~ in compliance with the License.
~ You may obtain a copy of the License at
~
~ http://www.apache.org/licenses/LICENSE-2.0
~
~ Unless required by applicable law or agreed to in writing,
~ software distributed under the License is distributed on an
~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
~ KIND, either express or implied. See the License for the
~ specific language governing permissions and limitations
~ under the License.
-->
<%@page import="org.wso2.carbon.identity.application.authentication.endpoint.oauth2.OAuth2Login"%>
<%@ page import="org.wso2.carbon.identity.application.authentication.endpoint.Constants" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String loggedInUser = OAuth2Login.getSafeText(request.getParameter("loggedInUser"));
    String scopeString = OAuth2Login.getSafeText(request.getParameter("scope"));
    String user = "";
    if (loggedInUser.split("/").length > 0){
        user = loggedInUser.split("/")[1].split("@")[0];
    }else{
        user = loggedInUser.split("@")[0];
    }
    String fullApp = request.getParameter("application");
    String app = fullApp;
    if (fullApp.split("_").length >= 4){
        Integer len = fullApp.split("_").length;
        app = "";
        for (int i=2; i<len-1; i++){
            if (i<len-2){
                app = app + fullApp.split("_")[i] + "_";
            }else{
                app = app + fullApp.split("_")[i];
            }
        }
    }
%>

<!DOCTYPE html>
 <html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Login with Cyverse</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
       <!-- <link href="assets/css/bootstrap.min.css" rel="stylesheet">-->

        <!-- CDN Bootstrap Styles -->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="css/bootstrap-3.3.6-dist/css/bootstrap.min.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="js/js/bootstrap.min.js"></script>

        <link href="css/localstyles.css" rel="stylesheet">
        <!--[if lt IE 8]>
        <link href="css/localstyles-ie7.css" rel="stylesheet">
        <![endif]-->

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src="assets/js/html5.js"></script>
        <![endif]-->
        <script src="assets/js/jquery-1.7.1.min.js"></script>
        <script src="js/scripts.js"></script>
        <script src="https://code.jquery.com/jquery-2.2.3.min.js" integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoE
Jo=" crossorigin="anonymous"></script>
        
                                                                                                           
    </head>                                                                                                
                                                                                                           
    <body>
    <div class="header-back">                                                                              
        <div class="container">                                                                            
            <div class="row">                                                                              
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">      
                    <a class="logo" href="http://www.cyverse.org" target="_blank"><img alt="Cyverse Logo" class="col-xs-12 co
l-sm-12 col-md-12 col-lg-12" title="Cyverse" src="images/cyverse_rgb.png"></a>
                </div>
<div class="">                                                            
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 content-section">
                        You are logged in as <strong><%=user%></strong>. <strong><%=app%></strong> requests access to your profile information
                    </div>                                                          
            </div>            
	    
	        </div>
	   </div>
    </div>
    
    <div class="" style="margin-top:10px;">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 content-section">
            <script type="text/javascript">
                    function approved() {
                        document.getElementById('consent').value="approve";
                        document.getElementById("oauth2_authz").submit();
                    }
                    function approvedAlways() {
                        document.getElementById('consent').value="approveAlways";
                        document.getElementById("oauth2_authz").submit();
                    }
                    function deny() {
                        document.getElementById('consent').value="deny";
                        document.getElementById("oauth2_authz").submit();
                    }
                </script>

                <form id="oauth2_authz" name="oauth2_authz" method="post" action="../oauth2/authorize">
                            <div class="buttonRow col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input type="button" class="btn btn-primary btn-large" id="approve" name="approve"
                                       onclick="javascript: approved(); return false;"
                                       value="Approve"/>
                                <input type="button" class="btn btn-primary btn-large" id="approveAlways" name="approveAlways"
                                       onclick="javascript: approvedAlways(); return false;"
                                       value="Approve Always"/>
                                <input class="btn btn-primary-deny btn-large" type="reset"
                                       value="Deny" onclick="javascript: deny(); return false;" />

                                <input type="hidden" name="<%=Constants.SESSION_DATA_KEY_CONSENT%>"
                                       value="<%=request.getParameter(Constants.SESSION_DATA_KEY_CONSENT)%>" />
                                <input type="hidden" name="consent" id="consent"
                                       value="deny" />
                </form>
            </div>
        </div>
   </div>
   
</body>
</html>

