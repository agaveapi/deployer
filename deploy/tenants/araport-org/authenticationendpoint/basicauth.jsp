<%@ page import="org.wso2.carbon.identity.application.authentication.endpoint.util.CharacterEncoder"%>
<div id="loginTable1" class="identity-box">
    <%
        loginFailed = CharacterEncoder.getSafeText(request.getParameter("loginFailed"));
        if (loginFailed != null) {

    %>
            <div class="alert alert-error alert-danger">
                <fmt:message key='<%=CharacterEncoder.getSafeText(request.getParameter
                ("errorMessage"))%>'/>
            </div>
    <% } %>

    <% if (CharacterEncoder.getSafeText(request.getParameter("username")) == null || "".equals
    (CharacterEncoder.getSafeText(request.getParameter("username")).trim())) { %>

        <!-- Username -->
        <div class="form-group">
            <label class="control-label" for="username"><fmt:message key='username'/></label>

            <div class="controls col-md-8">
                <input class="input-xlarge form-control" placeholder="User Name" label="User Name" type="text" id='username' name="username" size='30'/>
            </div>
        </div>

    <%} else { %>

        <input type="hidden" id='username' name='username' value='<%=CharacterEncoder.getSafeText
        (request.getParameter("username"))%>'/>

    <% } %>

    <!--Password-->
    <div class="form-group">
        <label class="control-label" for="password"><fmt:message key='password'/>&nbsp;</label>

        <div class="controls col-md-8">
            <input type="password" id='password' name="password"  class="form-control" placeholder="Password" size='30'/>
            <input type="hidden" name="sessionDataKey" value='<%=CharacterEncoder.getSafeText(request.getParameter("sessionDataKey"))%>'/>
      
        </div>
    </div>

    <div class="form-actions">
        <input type="submit" id="login-btn" value="Log In" class="btn btn-primary">
    </div>
	<br />

<div class="help-link help-block" id="helpBlock"><span class="forgot-password"><a href="https://www.araport.org/user/password" target="_blank">Forgot Password</a></span> | <a href="htt
p://status.agaveapi.co" target="_blank" title="Platform Status">Platform Status</div>



</div>

