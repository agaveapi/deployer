import os

c.JupyterHub.spawner_class = 'dockerspawner.DockerSpawner'

c.JupyterHub.confirm_no_ssl = True

import netifaces
docker0 = netifaces.ifaddresses('eth0')
docker0_ipv4 = docker0[netifaces.AF_INET][0]
c.JupyterHub.hub_ip = docker0_ipv4['addr']
c.JupyterHub.proxy_api_ip = '0.0.0.0'
c.DockerSpawner.container_ip = '172.17.0.1'
c.DockerSpawner.use_internal_ip = False
c.DockerSpawner.hub_ip_connect = c.JupyterHub.hub_ip

c.DockerSpawner.container_image = 'jstubbs/jupyter-pyspark'
c.DockerSpawner.remove_containers = True

c.SystemUserSpawner.host_homedir_format_string = '/home/apim'

# to spawn user containers in privileged mode:
# c.DockerSpawner.extra_host_config = {'privileged': True}

c.JupyterHub.authenticator_class = 'oauthenticator.AgaveOAuthenticator'

c.AgaveOAuthenticator.oauth_callback_url = os.environ['OAUTH_CALLBACK_URL']
c.AgaveOAuthenticator.client_id = os.environ['AGAVE_CLIENT_ID']
c.AgaveOAuthenticator.client_secret = os.environ['AGAVE_CLIENT_SECRET']
c.AgaveOAuthenticator.agave_base_url = os.environ['AGAVE_BASE_URL']
c.AgaveOAuthenticator.agave_base_url = os.environ['AGAVE_TENANT_NAME']

c.AgaveMixin.agave_base_url = os.environ['AGAVE_BASE_URL']