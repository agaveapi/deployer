#
# This file documents the configuration options for Agave's multi-tenant Jupyterhub.

# ---------------------
# GLOBAL CONFIGURATIONS
# ---------------------
#
# Configurations in this section apply to all tenants.
#
# image to use for jupyterhub
jupyterhub_image: taccsciapps/jupyterhub:dev

# image to use for the user's notebook server
jupyter_user_image: jstubbs/jupyter-pyspark

# Whether to use the included NFS solution for persistent storage.
# if true, this will create an nfs server on the manager host and nfs mounts for the token and user share directories
# allowing for persisting data across a cluster of nodes. Some tenants, such as designsafe, have a special host with a
# custom nfs. they should set this to false.
use_stock_nfs: true

# (internal) IP address to use for NFS mount. This should be an IP address that resolves to the same host as jupyterhub.
# Only required if "use_stock_nfs" is true.
nfs_mount_ip: 192.168.0.126

# (Not always required). The IP for the dockerspawner to use to spawn user server containers. We default this to
# 172.17.42.1, which seems to work on Docker 1.9, but on occasion have needed to override it to 172.17.0.1.
docker_spawner_container_ip: 172.17.0.1

# ---------------
# TENANTS SECTION
# ---------------
#
# This section configures all the tenants to be deployed.

jupyterhub_tenants:
# for each tenant, provide the following YAML collection
#    - tenant_id: id of the tenant. used for isolation: for example, to separate token cache files, user data directories, etc.
#      jupyter_host: URL where nginx will serve the tenant's hub.
#      jupyter_key_file: key file for jupyter host
#      jupyter_cert_file: cert file for jupyter host
#      port: port that this tenant's hub will be exposed on. chose something between 8000 and 8100 and make sure each
#            tenant's port is unique.
#      oauth_callback_url: the callback URL for the jupyter hub's oauth client
#      oauth_validate_cert: whether the oauth server's certificate should be validated (should be yes in production; no if self-signed)
#      agave_client_id: the id for the jupyter hub's oauth client
#      agave_client_secret: the secret for the jupyter hub's oauth client
#      agave_base_url: the base URL for the tenant
#      agave_tenant_name: (optional) name of the tenant. can be used to generate customized text/behavior in custom
#                         spawner or authenticator classed. Not used by default classes, however.
#      agave_login_button_text: (optional) customize the login button text.
