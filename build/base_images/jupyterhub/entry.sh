#!/bin/bash

echo "top of entry.sh for jupyterhub.."
echo "USE_STOCK_NFS="$USE_STOCK_NFS

if "${USE_STOCK_NFS,,}" == "true"
then
    echo "Using stock NFS..."
    # mount nfs
    mount -o nolock $NFS_MOUNT_IP:/home/apim /home/apim
    mount -o nolock $NFS_MOUNT_IP:/tokens /tokens
    # sleep to avoid races
    sleep 3

    # copy mounted login file
    cp $CONFIG_PATH/login.html /usr/local/share/jupyter/hub/templates/login.html
    cp $CONFIG_PATH/volume_mounts /volume_mounts
    cp $CONFIG_PATH/jupyterhub_config.py /srv/jupyterhub/jupyterhub_config.py
fi

# launch jupyterhub
jupyterhub -f /srv/jupyterhub/jupyterhub_config.py