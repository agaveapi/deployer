#!/bin/bash

./template_compiler/template_entry.sh

# We need to sleep on initial start up to give apim a chance to launch. This way, the haproxy instance doesn't notice
# the new stack until apim is ready.

sleep 90

/usr/sbin/apache2ctl -D FOREGROUND

