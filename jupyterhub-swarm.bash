#!/bin/bash
docker run --rm -ti \
    -v ${HOME}/keys/:/root/.ssh/ \
    -v ${PWD}/deploy/tenants/dev_sandbox/dev_sandbox_passwords:/deploy/tenants/dev_sandbox/dev_sandbox_passwords \
    -v ${PWD}/deploy/tenants/dev_sandbox/httpd:/deploy/tenants/dev_sandbox/httpd \
    -v ${PWD}/deploy/core-apis-ssl:/deploy/core-apis-ssl \
    -v ${PWD}/deploy/agave_core_configs/dev_sandbox_passwords:/deploy/agave_core_configs/dev_sandbox_passwords \
    agaveapi/deployer-ans212 -vvvv -i /deploy/host_files/jupyterhub_creation_hosts /deploy/create_jupyterhub_network.plbk
