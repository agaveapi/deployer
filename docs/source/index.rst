.. CoTIM documentation master file, created by
   sphinx-quickstart on Fri Jun 12 11:01:03 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Agave CoTIM - Container Tenant Infrastructure Manager
=====================================================

Contents:

.. toctree::
   :maxdepth: 2
   
   Introduction
   Build
   Deploy


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

